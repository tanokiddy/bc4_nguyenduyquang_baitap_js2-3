// BT1
//Mô hình 3 khối BT1.
//input: người dùng nhập tiền lương vào ô input Tiền Lương.

//Todo:
//- Tạo biến số ngày làm.
//- Tạo biến tiền lương trên 1 ngày.
//- Gán giá trị của 2 biến số ngày làm và tiền lương trên 1 ngày theo ô input ngươi dùng nhập.
// tạo biến tiền lương dựa trên công thức số ngày làm * với tiền lương trên 1 ngày.
// Console kết quả.

//Output: hiện kết quả tiền lương.

function tinhTienLuong() {
  var soNgay = document.getElementById("txt-so-ngay").value * 1;
  var luongNgay = document.getElementById("txt-luong-1-ngay").value * 1;
  // console.log({ soNgay, luongNgay });

  var tienLuong = soNgay * luongNgay;
  // console.log("tienLuong: ", tienLuong);
  document.getElementById("result1").innerHTML = `<h1> Total: ${tienLuong} VND
   </h1>`;
}

// BT2
//Mô hình 3 khối BT2
//Input: Người dùng nhập lần lượt các số vào ô từ 1-5.

//Todo:
//- Tạo biến của 5 số và gán giá trị cho 5 số theo gía trị được nhập từ ô input.
//- Tạo biến tính giá trị trung binh của 5 số theo công thức lấy tổng 5 số chia cho 5.
//- in kết quả ra Console.

//Output: hiển thị kết quả trung bình của 5 số do người dùng nhập.

function tinhTrungBinh() {
  var s1 = document.getElementById("so1").value * 1;
  var s2 = document.getElementById("so2").value * 1;
  var s3 = document.getElementById("so3").value * 1;
  var s4 = document.getElementById("so4").value * 1;
  var s5 = document.getElementById("so5").value * 1;
  // console.log({ s1, s2, s3, s4, s5 });
  var trungBinhSo = (s1 + s2 + s3 + s4 + s5) / 5;
  // console.log("trungBinhSo: ", trungBinhSo);
  document.getElementById("result2").innerHTML = `<h1> Total: ${trungBinhSo} 
     </h1>`;
}

// BT3
//Mô hình 3 khối BT3
//Input: người dùng nhập số tiền $ mà họ muốn quy đổi sang VNĐ.

//Todo:
//- Tạo biến số tiền $ người dùng muốn quy đổi và biến giá trị của 1$ theo tiền VNĐ.
//- Gán giá trị tương ứng như trên từ thông tin người dùng nhập vào ô input.
//- Tạo biến giá trị sau khi quy đổi từ $ sang VNĐ và gán theo công thức số tiền $ * giá trị $ quy đổi sang VNĐ.
//- In kết quả ra Console.

//Output: Hiển thị kết quả sau quy đổi

function tinhQuyDoi() {
  var soTien$ = document.getElementById("txt-sotiencandoi").value * 1;
  var giaTri$ = document.getElementById("txt-giatri$").value * 1;
  // console.log({ soTien$, giaTri$ });

  var tienVND = soTien$ * giaTri$;
  // console.log("tienVND: ", tienVND);
  document.getElementById("result3").innerHTML = `<h1> Total: ${tienVND} VND
   </h1>`;
}

// BT4
//Mô hình 3 khối BT4
//Input: Người dùng nhập giá trị chiều dài và chiều rộng hình CN.

//Todo:
//- Tạo biến chiều dài và chiều rồng.
//- Gán giá trị cho 2 biến trên.
//- Tạo biến Chu vi và biến diện tích, đồng thời gán giá trị là công thức tính chu vi và diện tích.
//- Áp dụng If else để loại trừ trường hợp người dùng nhập các giá trị không phù hợp cho chiều dài và chiều rộng của hình CN.
//- in kết quả tính chu vi và diện tích ra console.

//Output: Hiển thị kết quả chu vi và diện tích của hình CN.

function tinhChuVi() {
  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;
  // console.log({ chieuDai, chieuRong });

  var chuVi = 0;
  if (chieuRong >= chieuDai) {
    chuVi = NaN;
  } else {
    chuVi = (chieuDai + chieuRong) * 2;
    // console.log("chuVi: ", chuVi);
  }
  document.getElementById("resultchuvi").innerHTML = `<h1> P = ${chuVi} 
     </h1>`;
}
function tinhDienTich() {
  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;

  var dienTich = 0;
  if (chieuRong >= chieuDai) {
    dienTich = NaN;
  } else {
    dienTich = chieuDai * chieuRong;
    // console.log("dienTich: ", dienTich);
  }
  document.getElementById("resultdientich").innerHTML = `<h1> S = ${dienTich} 
     </h1>`;
}

// BT5
//Mô hình 3 khối BT5
//Input: người dùng nhập số có 2 chữ số vào ô input.

//Todo:
//- Tạo biến số có 2 chữ số và gán giá trị theo ô input người dùng nhập.
//- tạo biến tính tổng ký số và gán công thức tính tổng ký số vào.
//- Dùng if else để loại trừ trường hợp người dùng nhập sai số có 2 chữ số thành số có 1 chữ số.
//- In kết quả ra console.

//Output: Hiển thị kết quả tổng ký số từ số có 2 chữ số người dùng nhập ban đầu.

function tinhTongKySo() {
  var so2ChuSo = document.getElementById("txt-so-2-chu-so").value * 1;
  var tongKySo = 0;
  if (so2ChuSo < 10) {
    so2ChuSo = NaN;
    tongKySo = NaN;
  } else {
    so2ChuSo = document.getElementById("txt-so-2-chu-so").value * 1;
    tongKySo = (so2ChuSo % 10) + Math.floor(so2ChuSo / 10);
    // console.log({ so2ChuSo, tongKySo });
  }
  document.getElementById(
    "result5"
  ).innerHTML = `<h1> Tổng ký số: ${tongKySo}</h1>`;
}
